package com.epam.trainings.controller;

import com.epam.trainings.model.BinaryTreeMap.BinaryTreeMap;
import com.epam.trainings.model.CustomPriorityQueue.CustomPriorityQueue;
import com.epam.trainings.model.Deque.CustomDeque;
import com.epam.trainings.view.View;

import java.util.*;

public class ViewController {

  private PriorityQueueController priorityQueueController;
  private BinaryTreeMapController bMapController;
  private DequeController dequeController;

  public ViewController(CustomPriorityQueue pQueue, BinaryTreeMap bMap, CustomDeque deque) {
    priorityQueueController = new PriorityQueueController(pQueue);
    bMapController = new BinaryTreeMapController(bMap);
    dequeController = new DequeController(deque);
  }

  public PriorityQueueController getPriorityQueueController() {
    return priorityQueueController;
  }

  public BinaryTreeMapController getbMapController() {
    return bMapController;
  }

  public DequeController getDequeController() {
    return dequeController;
  }

  public final class PriorityQueueController<E extends Comparable<E>> extends AbstractQueue<E> {

    CustomPriorityQueue<E> priorityQueue;

    PriorityQueueController(CustomPriorityQueue queue) {
      this.priorityQueue = queue;
    }

    @Override
    public Iterator iterator() {
      return priorityQueue.iterator();
    }

    @Override
    public int size() {
      return priorityQueue.size();
    }

    @Override
    public boolean offer(E o) {
      return priorityQueue.offer(o);
    }

    @Override
    public E poll() {
      return priorityQueue.poll();
    }

    @Override
    public E peek() {
      return priorityQueue.peek();
    }

    public boolean add(E e) {
      return priorityQueue.add(e);
    }
  }

  public final class DequeController<E> implements DequeInterface<E> {

    CustomDeque<E> deque;

    DequeController(CustomDeque deque) {
      this.deque = deque;
    }

    @Override
    public void addFirst(E e) {
      deque.addFirst(e);
    }

    @Override
    public void addLast(E e) {
      deque.addLast(e);
    }

    @Override
    public boolean isEmpty() {
      return deque.isEmpty();
    }

    @Override
    public boolean offerFirst(E e) {
      return deque.offerFirst(e);
    }

    @Override
    public boolean offerLast(E e) {
      return deque.offerLast(e);
    }

    @Override
    public E removeFirst() {
      return deque.removeFirst();
    }

    @Override
    public E removeLast() {
      return deque.removeLast();
    }

    @Override
    public E pollFirst() {
      return deque.pollFirst();
    }

    @Override
    public E pollLast() {
      return deque.pollLast();
    }

    @Override
    public E getFirst() {
      return deque.getFirst();
    }

    @Override
    public E getLast() {
      return deque.getLast();
    }

    @Override
    public boolean add(E e) {
      return deque.add(e);
    }

    @Override
    public E remove() {
      return deque.remove();
    }

    @Override
    public E poll() {
      return deque.poll();
    }

    @Override
    public void push(E e) {
      deque.push(e);
    }

    @Override
    public E pop() {
      return deque.pop();
    }

    @Override
    public boolean remove(Object o) {
      return deque.remove(o);
    }

    @Override
    public int size() {
      return deque.size();
    }
  }

  public final class BinaryTreeMapController<K, V> implements Map<K, V> {

    BinaryTreeMap<K, V> bmap;

    BinaryTreeMapController(BinaryTreeMap bmap) {
      this.bmap = bmap;
    }

    @Override
    public int size() {
      return bmap.size();
    }

    @Override
    public boolean isEmpty() {
      return bmap.isEmpty();
    }

    @Override
    public boolean containsKey(Object key) {
      return bmap.containsKey(key);
    }

    @Override
    public boolean containsValue(Object value) {
      return bmap.containsValue(value);
    }

    @Override
    public V get(Object key) {
      return bmap.get(key);
    }

    @Override
    public V put(K key, V value) {
      return bmap.put(key, value);
    }

    @Override
    public V remove(Object key) {
      return bmap.remove(key);
    }

    @Override
    public void putAll(Map<? extends K, ? extends V> m) {
      bmap.putAll(m);
    }

    @Override
    public void clear() {
      bmap.clear();
    }

    @Override
    public Set<K> keySet() {
      return bmap.keySet();
    }

    @Override
    public Collection<V> values() {
      return bmap.values();
    }

    @Override
    public Set<Entry<K, V>> entrySet() {
      return bmap.entrySet();
    }
  }
}
