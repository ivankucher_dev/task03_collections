package com.epam.trainings.controller;

import java.util.Collection;
import java.util.Iterator;

public interface DequeInterface<E> {

  void addFirst(E e);

  void addLast(E e);

  boolean isEmpty();

  boolean offerFirst(E e);

  boolean offerLast(E e);

  E removeFirst();

  E removeLast();

  E pollFirst();

  E pollLast();

  E getFirst();

  E getLast();

  boolean add(E e);

  E remove();

  E poll();

  void push(E e);

  E pop();

  boolean remove(Object o);

  int size();
}
