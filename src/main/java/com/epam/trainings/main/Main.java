package com.epam.trainings.main;

import com.epam.trainings.controller.ViewController;
import com.epam.trainings.model.BinaryTreeMap.BinaryTreeMap;
import com.epam.trainings.model.CustomPriorityQueue.CustomPriorityQueue;
import com.epam.trainings.model.Deque.CustomDeque;
import com.epam.trainings.view.View;

import java.util.PriorityQueue;

public class Main {
  public static void main(String[] args) {
    CustomPriorityQueue pQueue = new CustomPriorityQueue();
    BinaryTreeMap bMap = new BinaryTreeMap();
    CustomDeque deque = new CustomDeque();
    ViewController controller = new ViewController(pQueue, bMap, deque);
    View view = new View(controller);
  }
}
