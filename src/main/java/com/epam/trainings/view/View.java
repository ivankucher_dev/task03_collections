package com.epam.trainings.view;

import com.epam.trainings.controller.ViewController;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.Scanner;

public class View {

  private static final int MAP_MENU = 1;
  private static final int QUEUE_MENU = 3;
  private static final int DEQUE_MENU = 2;
  private static final int MAP_MENU_ADD = 1;
  private static final int MAP_MENU_REMOVE = 2;
  private static final int DEQUE_MENU_ADD_FIRST = 1;
  private static final int DEQUE_MENU_ADD_LAST = 2;
  private static final int DEQUE_MENU_REMOVE = 3;
  private static final int QUEUE_MENU_ADD = 1;
  private static final int QUEUE_MENU_REMOVE = 2;
  private static final int END = 5;
  private static Logger log = LogManager.getLogger(View.class.getName());
  ViewController viewController;

  public View(ViewController viewController) {
    this.viewController = viewController;
    initView();
  }

  public void initView() {
    log.info("Init view : \n1)work with binary map\n2)work with deque\n3)work with priority");
    int choice;
    Scanner scan = new Scanner(System.in);
    do {
      choice = scan.nextInt();
      switch (choice) {
        case MAP_MENU:
          callBinaryTreeMenu();
          break;
        case DEQUE_MENU:
          callDequeMenu();
          break;

        case QUEUE_MENU:
          callPriorityQueueMenu();
          break;
      }
    } while (choice != END);
  }

  public void callPriorityQueueMenu() {
    log.info("Priority queue work menu (enter 5 to exit) : ");
    int choice;
    Scanner scan = new Scanner(System.in);
    do {
      choice = scan.nextInt();
      switch (choice) {
        case QUEUE_MENU_ADD:
          log.info(
              "Add 10 to priority queue " + viewController.getPriorityQueueController().add(10));
          log.info(
              "Add 20 to priority queue " + viewController.getPriorityQueueController().add(20));
          log.info("Add 5 to priority queue " + viewController.getPriorityQueueController().add(5));
          log.info(viewController.getPriorityQueueController().iterator());
          break;

        case QUEUE_MENU_REMOVE:
          log.info(
              "Remove 20 from priority queue "
                  + viewController.getPriorityQueueController().add(20));
          log.info(
              "Remove 5 from priority queue " + viewController.getPriorityQueueController().add(5));
          log.info(
              "Remove 10 from priority queue "
                  + viewController.getPriorityQueueController().add(10));
          log.info(viewController.getPriorityQueueController().iterator());
          break;
      } // end of switch
    } while (choice != END); // end of loop
  }

  public void callBinaryTreeMenu() {
    log.info("Welcome to binary map work menu ( press 5 to exit) : ");
    int choice;
    Scanner scan = new Scanner(System.in);
    do {
      choice = scan.nextInt();
      switch (choice) {
        case MAP_MENU_ADD:
          System.out.println();
          log.info("Add [5-Welcome] to map" + viewController.getbMapController().put(5, "Welcome"));
          log.info("Add [7-Rest] to map" + viewController.getbMapController().put(7, "Rest"));
          log.info("Add [3-Der] to map" + viewController.getbMapController().put(3, "Der"));
          log.info("Add [4-Devil] to map" + viewController.getbMapController().put(4, "Devil"));
          log.info("Size = " + viewController.getbMapController().size() + 1);
          break;

        case MAP_MENU_REMOVE:
          System.out.println();
          log.info("Remove [5-Welcome] from map" + viewController.getbMapController().remove(5));
          log.info("Remove [3-Der] from map" + viewController.getbMapController().remove(3));
          log.info("Size = " + viewController.getbMapController().size() + 1);
          break;
      } // end of switch
    } while (choice != END); // end of loop
  }

  public void callDequeMenu() {
    log.info("Welcome to deque work menu ( press 5 to exit) : ");
    int choice;
    Scanner scan = new Scanner(System.in);
    do {
      choice = scan.nextInt();
      switch (choice) {
        case DEQUE_MENU_ADD_FIRST:
          log.info("Put 3 sentences to first");
          viewController.getDequeController().addFirst("Something");
          viewController.getDequeController().addFirst("That");
          viewController.getDequeController().addFirst("We put");
          log.info("Size now " + viewController.getDequeController().size());
          break;

        case DEQUE_MENU_ADD_LAST:
          log.info("Put 2 sentences to last");
          viewController.getDequeController().addFirst("Ok");
          viewController.getDequeController().addFirst("Add here");
          break;

        case 4:
          log.info("Get from first " + viewController.getDequeController().getFirst());
          log.info("Get from last " + viewController.getDequeController().getLast());

        case DEQUE_MENU_REMOVE:
          log.info("Remove " + viewController.getDequeController().remove());
      } // end of switch
    } while (choice != END); // end of loop
  }
}
