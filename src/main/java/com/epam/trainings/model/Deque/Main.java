package com.epam.trainings.model.Deque;

import com.epam.trainings.controller.ViewController;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class Main {
  private static Logger log = LogManager.getLogger(Main.class.getName());
  private static final String TEST_ADD_FIRST = " [TEST ADD FIRST] ";
  private static final String TEST_ADD_LAST = " [TEST ADD LAST] ";
  private static final String TEST_REMOVE_FIRST = " [TEST REMOVE FIRST] ";
  private static final String TEST_REMOVE_LAST = " [TEST REMOVE LAST] ";
  private static final String TEST_GET_FIRST = " [TEST GET FIRST] ";
  private static final String TEST_GET_LAST = " [TEST GET LAST] ";

  public static void main(String[] args) {
    CustomDeque<Integer> deque = new CustomDeque<>();
    log.info(TEST_ADD_FIRST + " 15");
    deque.addFirst(15);
    log.info(TEST_GET_FIRST + deque.getFirst());
    log.info(TEST_ADD_LAST + " 25");
    deque.addLast(25);
    log.info(TEST_GET_LAST + deque.getLast());

    log.info(TEST_ADD_FIRST + " 45");
    deque.addFirst(45);
    log.info(TEST_GET_FIRST + deque.getFirst());
    log.info(TEST_ADD_LAST + " 32");
    deque.addLast(32);
    log.info(TEST_GET_LAST + deque.getLast());
    log.info(TEST_REMOVE_FIRST + deque.removeFirst());
    log.info(TEST_GET_FIRST + deque.getFirst());
    log.info(TEST_REMOVE_LAST + deque.removeLast());
    log.info(TEST_GET_LAST + deque.getLast());
  }
}
