package com.epam.trainings.model.Deque;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import java.util.Collection;
import java.util.Deque;
import java.util.Iterator;

public class CustomDeque<E> implements Deque<E> {

  private static Logger log = LogManager.getLogger(CustomDeque.class.getName());
  public Object[] elements;
  private int head;
  private int tail;
  private int size;
  private static int MIN_CAPACITY = 5;

  public CustomDeque() {
    log.info("Initializing Deque with head = -1 and tail = 0 and size = " + MIN_CAPACITY);
    head = -1;
    tail = 0;
    size = MIN_CAPACITY;
    elements = new Object[MIN_CAPACITY];
  }

  @Override
  public void addFirst(E e) {
    fixOverflowCapacity();
    if (e == null) throw new IllegalArgumentException();
    if (head == -1) {
      head = 0;
    } else if (head == 0) {
      head = size - 1;
    } else {
      head -= 1;
    }
    elements[head] = e;
  }

  @Override
  public void addLast(E e) {
    fixOverflowCapacity();
    if (head == -1) {
      tail = 0;
    }
    if (tail == size - 1) {
      tail = 0;
    } else {
      tail = tail + 1;
    }
    elements[tail] = e;
  }

  public boolean isEmpty() {
    return (head == -1);
  }

  @Override
  public boolean offerFirst(E e) {
    addFirst(e);
    return true;
  }

  @Override
  public boolean offerLast(E e) {
    addLast(e);
    return true;
  }

  @Override
  public E removeFirst() {
    if (isEmpty()) {
      return null;
    }
    Object result = elements[head];
    if (head == size - 1) {
      deleteElementFromArray(head);
      head = 0;
    } else {
      deleteElementFromArray(head);
      head = head + 1;
    }
    return (E) result;
  }

  @Override
  public E removeLast() {
    if (isEmpty()) {
      return null;
    }
    Object result = elements[tail];
    if (tail == 0) {
      deleteElementFromArray(tail);
      tail = size - 1;
    } else {
      deleteElementFromArray(tail);
      tail = tail - 1;
    }
    return (E) result;
  }

  @Override
  public E pollFirst() {
    E result = getFirst();
    deleteElementFromArray(head);
    return result;
  }

  @Override
  public E pollLast() {
    E result = getLast();
    deleteElementFromArray(tail);
    return result;
  }

  @Override
  public E getFirst() {
    return (E) elements[head];
  }

  @Override
  public E getLast() {
    return (E) elements[tail];
  }

  @Override
  public E peekFirst() {
    return null;
  }

  @Override
  public E peekLast() {
    return null;
  }

  @Override
  public boolean removeFirstOccurrence(Object o) {
    return false;
  }

  @Override
  public boolean removeLastOccurrence(Object o) {
    return false;
  }

  @Override
  public boolean add(E e) {
    addLast(e);
    return true;
  }

  @Override
  public boolean offer(E e) {
    return offerLast(e);
  }

  @Override
  public E remove() {
    return removeFirst();
  }

  @Override
  public E poll() {
    return null;
  }

  @Override
  public E element() {
    return null;
  }

  @Override
  public E peek() {
    return null;
  }

  @Override
  public void push(E e) {
    addFirst(e);
  }

  @Override
  public E pop() {
    return removeFirst();
  }

  @Override
  public boolean remove(Object o) {
    return false;
  }

  @Override
  public boolean containsAll(Collection<?> c) {
    return false;
  }

  @Override
  public boolean addAll(Collection<? extends E> c) {
    return false;
  }

  @Override
  public boolean removeAll(Collection<?> c) {
    return false;
  }

  @Override
  public boolean retainAll(Collection<?> c) {
    return false;
  }

  @Override
  public void clear() {}

  @Override
  public boolean contains(Object o) {
    return false;
  }

  @Override
  public int size() {
    return size;
  }

  @Override
  public Iterator<E> iterator() {
    return null;
  }

  @Override
  public Object[] toArray() {
    return elements.clone();
  }

  @Override
  public <T> T[] toArray(T[] a) {
    return null;
  }

  @Override
  public Iterator<E> descendingIterator() {
    return null;
  }

  private void doubleCapacity() {
    int start = head;
    int n = elements.length;
    int r = n - start; // number of elements to the right of p
    int newCapacity = n << 1;
    if (newCapacity < 0) throw new IllegalStateException("Sorry, deque too big");
    Object[] a = new Object[newCapacity];
    System.arraycopy(elements, start, a, 0, r);
    System.arraycopy(elements, 0, a, r, start);
    elements = a;
    head = 0;
    tail = n - 1;
    size = elements.length;
    log.info("Double capacity , size = " + size);
  }

  private void fixOverflowCapacity() {
    if ((head == 0 && tail == size - 1) || head == tail + 1) {
      log.info("Fix overflow capacity , size before doubleCapacity call = " + size);
      doubleCapacity();
    }
  }

  private void deleteElementFromArray(int index) {
    elements[index] = null;
  }
}
