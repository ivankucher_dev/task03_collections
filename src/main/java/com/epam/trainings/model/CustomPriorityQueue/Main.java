package com.epam.trainings.model.CustomPriorityQueue;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class Main {
  private static Logger log = LogManager.getLogger(Main.class.getName());
  private static final String TEST_POLL = " [TEST POLL] ";
  private static final String TEST_REMOVE = " [TEST REMOVE] ";
  private static final String TEST_ADD = " [TEST ADD] ";
  private static final String TEST_PEEK = " [TEST PEEK] ";

  public static void main(String[] args) {

    CustomPriorityQueue priorityQueue = new CustomPriorityQueue();
    log.info(TEST_ADD + " 5 " + priorityQueue.add(5));
    log.info(TEST_ADD + " 6 " + priorityQueue.add(6));
    log.info(TEST_ADD + " 1 " + priorityQueue.add(1));
    log.info(TEST_ADD + " 2 " + priorityQueue.add(2));
    log.info(TEST_ADD + " 18 " + priorityQueue.add(18));
    log.info(TEST_POLL + priorityQueue.poll());
    log.info(TEST_POLL + priorityQueue.poll());
    log.info(TEST_PEEK + priorityQueue.peek());
    log.info(TEST_REMOVE + " [2] " + priorityQueue.remove(2));
    log.info("Getting everything from queue");
    for (Object e : priorityQueue.toArrayList()) {
      System.out.print(e + " ");
    }
  }
}
