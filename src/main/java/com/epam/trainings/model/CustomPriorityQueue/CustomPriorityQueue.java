package com.epam.trainings.model.CustomPriorityQueue;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.*;

public class CustomPriorityQueue<E extends Comparable<E>> extends AbstractQueue<E> {
  private static Logger log = LogManager.getLogger(CustomPriorityQueue.class.getName());
  private ArrayList<E> queue;
  private int size = 0;
  private Comparator<E> comparator;

  public CustomPriorityQueue() {
    log.info("Initalizing constructor with size 0");
    size = 0;
    queue = new ArrayList<>();
    this.comparator = Comparator.naturalOrder();
  }

  public boolean add(E e) {
    return offer(e);
  }

  @Override
  public Iterator<E> iterator() {
    return this.queue.iterator();
  }

  @Override
  public int size() {
    return size;
  }

  @Override
  public boolean offer(E e) {
    if (isEmpty()) {
      queue.add(e);
      size++;
      return true;
    }
    queue.add(e);
    size++;
    Collections.sort(queue, comparator);
    return true;
  }

  @Override
  public boolean remove(Object o) {
    if (queue.contains(o)) {
      queue.remove(o);
      return true;
    } else {
      log.info("Queue not contains element to remove");
      return false;
    }
  }

  @Override
  public E poll() {
    E result = queue.get(0);
    queue.remove(result);
    return result;
  }

  @Override
  public E peek() {
    return size == 0 ? null : queue.get(0);
  }

  public boolean isEmpty() {
    return size == 0 ? true : false;
  }

  public ArrayList<E> toArrayList() {
    return queue;
  }
}
