package com.epam.trainings.model.BinaryTreeMap;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class Main {
    private static Logger log = LogManager.getLogger(Main.class.getName());
  private static final String TEST_PUT = " [TEST PUT] ";
  private static final String TEST_EMPTY = " [TEST IS EMPTY] ";
  private static final String TEST_REMOVE = " [TEST REMOVE] ";
  private static final String TEST_GET = " [TEST GET] ";

    public static void main(String[] args) {
        testBinaryTree();
  }
  private static void testBinaryTree(){
      BinaryTreeMap<Integer,String> binaryTreeMap  = new BinaryTreeMap<>();
      log.info(TEST_EMPTY +"is empty : "+ binaryTreeMap.isEmpty());
      log.info(TEST_PUT+ binaryTreeMap.put(50,"Hello"));
      log.info(TEST_PUT+ binaryTreeMap.put(30,"From Main"));
      log.info(TEST_PUT+ binaryTreeMap.put(70,"Simple test"));
      log.info(TEST_PUT+ binaryTreeMap.put(20,"For map"));
      log.info(TEST_PUT+ binaryTreeMap.put(40,"Implemention"));
      log.info(TEST_PUT+ binaryTreeMap.put(60,"Of"));
      log.info(TEST_PUT+binaryTreeMap.put(80,"Map"));
      log.info("Size after all puts "+binaryTreeMap.size());
      log.info(TEST_REMOVE+ " remove 50");
      binaryTreeMap.remove(50);
      log.info(TEST_REMOVE+ " remove 20");
      binaryTreeMap.remove(20);
      log.info(TEST_EMPTY +"is empty : "+ binaryTreeMap.isEmpty());
      log.info("Size now "+binaryTreeMap.size());
      log.info(TEST_GET+" getting 50 - "+binaryTreeMap.get(50));
      log.info(TEST_GET+" getting 30 ");
      binaryTreeMap.get(30);
      log.info(TEST_GET+" trying to get null "+binaryTreeMap.get(null));
  }
}
