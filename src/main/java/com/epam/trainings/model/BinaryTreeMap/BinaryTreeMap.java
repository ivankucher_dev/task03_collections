package com.epam.trainings.model.BinaryTreeMap;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.*;

public class BinaryTreeMap<K, V> implements Map<K, V> {

  public BinaryTreeMap.Entry<K, V> root;
  private int size = 0;
  private static Logger log = LogManager.getLogger(BinaryTreeMap.class.getName());

  public BinaryTreeMap() {
    log.info("Initializing Binary Tree Map");
  }

  public int size() {
    return size;
  }

  public boolean isEmpty() {
    return size == 0 ? true : false;
  }

  public boolean containsKey(Object key) {
    return get(key) == null ? false : true;
  }

  public boolean containsValue(Object value) {
    return false;
  }

  public V get(Object key) {
    Entry<K, V> t = root;
    nullKeyCheck(key);
    Comparable<? super K> k = (Comparable<? super K>) key;
    while (t != null) {
      int cmp = k.compareTo(t.key);
      if (cmp > 0) {
        t = t.right;
      } else if (cmp < 0) {
        t = t.left;
      } else {
        log.info("Got value [" + t.getValue() + "] on key = " + t.getKey());
        return t.getValue();
      }
    }
    return null;
  }

  public V put(K key, V value) {
    if (root == null) {
      nullKeyCheck(key);
      root = new BinaryTreeMap.Entry<>(key, value);
      return root.value;
    }
    Entry<K, V> entry = recursivePut(root, new BinaryTreeMap.Entry<>(key, value));
    if (entry != null) {
      return value;
    }
    return null;
  }

  public V remove(Object key) {
    Entry<K, V> t = root;
    V value = null;
    if (t == null) {
      log.info("Map is empty");
      return null;
    }
    Comparable<? super K> k = (Comparable<? super K>) key;
    root = deleteAtRoot(t, key, k, value);
    size--;
    log.info("Removed successfully");
    return value;
  }

  private Entry<K, V> deleteAtRoot(Entry<K, V> root, Object key, Comparable<? super K> k, V value) {
    if (root == null) {
      return root;
    }
    int cmp = k.compareTo(root.key);
    if (cmp < 0) root.left = deleteAtRoot(root.left, key, k, value);
    else if (cmp > 0) root.right = deleteAtRoot(root.right, key, k, value);
    else {
      if (root.left == null) return root.right;
      else if (root.right == null) return root.left;

      root.key = minValue(root.right);

      root.right = deleteAtRoot(root.right, root.key, k, value);
    }

    return root;
  }

  private K minValue(Entry<K, V> root) {
    K minv = root.key;
    while (root.left != null) {
      minv = root.left.key;
      root = root.left;
    }
    return minv;
  }

  public void putAll(Map<? extends K, ? extends V> m) {}

  public void clear() {}

  public Set<K> keySet() {
    return null;
  }

  public Collection<V> values() {
    return null;
  }

  public Set<Map.Entry<K, V>> entrySet() {
    return null;
  }

  private Entry<K, V> recursivePut(Entry<K, V> current, Entry<K, V> toPut) {
    if (current == null) {
      Entry<K, V> e = new Entry<>(toPut.key, toPut.value);
      current = e;
      size++;
      return current;
    }
    Comparable<? super K> k = (Comparable<? super K>) current.key;
    int cmp = k.compareTo(toPut.key);
    if (cmp > 0) {
      current.left = recursivePut(current.left, toPut);
    } else if (cmp < 0) {
      current.right = recursivePut(current.right, toPut);
    } else {
      current.setValue(toPut.value);
      return toPut;
    }
    return current;
  }

  private final void nullKeyCheck(Object key) {
    if (key == null) {
      log.error("Key cannot be null");
      throw new NullPointerException();
    }
  }

  static final class Entry<K, V> implements Map.Entry<K, V> {
    K key;
    V value;
    BinaryTreeMap.Entry<K, V> left;
    BinaryTreeMap.Entry<K, V> right;

    Entry(K key, V value) {
      this.key = key;
      this.value = value;
      right = null;
      left = null;
    }

    public K getKey() {
      return key;
    }

    public V getValue() {
      return value;
    }

    public V setValue(V value) {
      V oldValue = this.value;
      this.value = value;
      return oldValue;
    }

    public boolean equals(Object o) {
      if (!(o instanceof Map.Entry)) return false;
      Map.Entry<?, ?> e = (Map.Entry<?, ?>) o;

      return valEquals(key, e.getKey()) && valEquals(value, e.getValue());
    }

    public int hashCode() {
      int keyHash = (key == null ? 0 : key.hashCode());
      int valueHash = (value == null ? 0 : value.hashCode());
      return keyHash ^ valueHash;
    }

    public String toString() {
      return key + "=" + value;
    }

    private final boolean valEquals(Object o1, Object o2) {
      return (o1 == null ? o2 == null : o1.equals(o2));
    }
  }
}
